package com.jsite.modules.aop;

/**
 * @author liuruijun
 * @date 2020-04-15
 */

public enum DBTypeEnum {
    MASTER, SLAVE1, SLAVE2;
}
