package com.jsite.modules.flowable.entity;

import java.util.List;

public class FieldAuthorize {

    private String fieldName;
    private String varName;
    private List<TaskNode> taskNodeList;

    public FieldAuthorize() {}
    public FieldAuthorize(String fieldName, String varName, List<TaskNode> taskNodeList) {
        this.fieldName = fieldName;
        this.varName = varName;
        this.taskNodeList = taskNodeList;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }

    public List<TaskNode> getTaskNodeList() {
        return taskNodeList;
    }

    public void setTaskNodeList(List<TaskNode> taskNodeList) {
        this.taskNodeList = taskNodeList;
    }
}
